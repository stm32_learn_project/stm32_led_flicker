#include "stm32f10x.h"
#include "led.h"
#include "delay.h"
#include "key.h"
#include "stdio.h"
#include "key_behavior.h"


/**
 * @name: KEY0_EXTIn_IRQHandler
 * @description: exti中断处理函数
 * @param {*}
 * @return {*}
 */
  uint8_t is_rising = 0;
typedef enum LedStuts {
    stuts_1 = 1, 
    stuts_2 = 2,
		stuts_3 = 3   
} LedStuts;
static u8 stuts=1;
void KEY0_EXTIn_IRQHandler(void)
{
    /* 局部静态变量，用于触发沿判断 */
   
		if (stuts==3){
			stuts++;
		}
    switch (stuts)
    {
			case 1:
        {/* Falling Trigger */
        EXTI_ITConfig(KEY0_EXTI_Line, DISABLE); /* 关EXTI线中断 */
        delay_ms(10);   /* 去抖 */
        if(GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0) == KEY0_DOWN)   /* 是否按下 */
        {
            /* 配置下次上升沿触发 */
            KEY_EXTI_Config(KEY0_EXTI_Line, EXTI_Trigger_Rising, ENABLE);
            is_rising = 1;
						led_bright_to_weak();
//            fprintf("worker1 %s",is_rising);//工作函数
        }
        else
        {
            EXTI_ITConfig(KEY0_EXTI_Line, ENABLE);
        }
			}
		
			case 2:
			{
        /* Rising Trigger */
        EXTI_ITConfig(KEY0_EXTI_Line, DISABLE); /* 关EXTI线中断 */
        delay_ms(10); /* 去抖 */
        if(GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0) == KEY0_UP) /* 是否松开 */
        {
            /* 配置下次下降沿触发 */
            KEY_EXTI_Config(KEY0_EXTI_Line, EXTI_Trigger_Falling, ENABLE);
            is_rising = 0;

           led_breathing_lamp();
//            sprintf("worker1 %s",is_rising);//工作函数
        }
        else
        {
            EXTI_ITConfig(KEY0_EXTI_Line, ENABLE);
        }
			}
		};
    EXTI_ClearITPendingBit(KEY0_EXTI_Line);
}
