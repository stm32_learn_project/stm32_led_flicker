#include "stm32f10x.h"
#include "led.h"
#include  "delay.h"
void LED_Init(void)
{	
	//1.打开控制GPIOC的时钟(APB2)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	//2.配置结构体	
	GPIO_InitTypeDef led_init;
	led_init.GPIO_Pin   = GPIO_Pin_2;      //GPIOB13引脚
	led_init.GPIO_Mode  = GPIO_Mode_Out_PP; //推挽输出	
	led_init.GPIO_Speed = GPIO_Speed_10MHz; //10MHz
	
	//3.对成员进行初始化
	GPIO_Init(GPIOB, &led_init);
	
	//初始化拉低电压
	GPIO_SetBits(GPIOB, GPIO_Pin_2);
}

void led_breathing_lamp(void)//呼吸灯
{ 
	u8 x=0,i=0,last_stust=0;
	u16 t = 1;
	for ( x=0 ;x<20;x++){
		u16 ii=0;
		for (;ii<1000;ii++){
		if(last_stust == 1) //LED 亮>暗
			{
				for(i=0;i<10;i++)
				{
					GPIO_ResetBits(GPIOB, GPIO_Pin_2); 	//LED灭
					delay_us(t);
					GPIO_SetBits(GPIOB,GPIO_Pin_2);		//LED亮
					delay_us(501-t);
				}
				t++;
				if (t>=500){
				last_stust=0;
				}
			}
			else //LED 暗>亮
			{
				for(i=0;i<10;i++)
				{	
					GPIO_SetBits(GPIOB,GPIO_Pin_2);		//LED亮
					delay_us(501-t);
					GPIO_ResetBits(GPIOB, GPIO_Pin_2); 	//LED灭
					delay_us(t);
				}
				t--;
				if (t==0){
				last_stust=1;
				}		
			}		
		}
	}
}
void led_bright_to_weak(void)//亮变弱
{	
	u8 x=0,i=0;
	u16 t = 1;
	for ( x=0;x<20;x++)	{
			if (t==500){t=0;}
			for(i=0;i<10;i++)
			{
				GPIO_ResetBits(GPIOB, GPIO_Pin_2); 	//LED灭
				delay_us(t);
				GPIO_SetBits(GPIOB,GPIO_Pin_2);		//LED亮
				delay_us(501-t);
			}
			t++;	
		
		}
}
