#include "stm32f10x.h"
#include "key.h"
#include  "delay.h"
#include "stdio.h"

void KEY_GPIO_Init(void)
{	
	//1.打开控制GPIOC的时钟(APB2)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	//2.配置结构体	
	GPIO_InitTypeDef key_init;
	key_init.GPIO_Pin   = GPIO_Pin_0;      //GPIOA0引脚
	key_init.GPIO_Mode  = GPIO_Mode_IPU; //上拉输入	

	
	//3.对成员进行初始化
	GPIO_Init(GPIOA, &key_init);
	
	//初始化拉低电压
	GPIO_ResetBits(GPIOA, GPIO_Pin_0);
}


//#if KEY_INTERRUPT_MODE
///**
// * @name: KEY_EXTI_Init
// * @description: 按键外部中断初始化
// * @param {*}
// * @return {*}
// */
void KEY_EXTI_Init(void)
{

    EXTI_InitTypeDef EXTI_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;
    /* exti 开启afio时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    EXTI_DeInit();
    /* exti中断IO口配置 */
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
    /* exti配置 */
    EXTI_InitStruct.EXTI_Line = EXTI_Line0;
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStruct);

    EXTI_ClearITPendingBit(EXTI_Line0);

    /* exti中断nvic配置 */
    NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);
}


/**
 * @name: EXTI_ITConfig
 * @description: 开关exti中断
 * @param {u32} EXTI_LINEn exti中断线
 * @param {FunctionalState} state exti中断状态
 * @return {*}
 */
 void EXTI_ITConfig(u32 EXTI_LINEn, FunctionalState state)
{
    if(state)
        EXTI->IMR |= EXTI_LINEn;
    else
        EXTI->IMR &= ~EXTI_LINEn;

}

/**
 * @name: KEY_EXTI_Config
 * @description: exti结构体配置
 * @param {u32} EXTI_LINEn  exti线
 * @param {EXTITrigger_TypeDef} EXTITrigger_x   exti触发模式
 * @param {FunctionalState} state  exti中断使能
 * @return {*}
 */
 void KEY_EXTI_Config(u32 EXTI_LINEn, EXTITrigger_TypeDef EXTITrigger_x, FunctionalState state)
{
    EXTI_InitTypeDef EXTI_InitStruct;

    EXTI_InitStruct.EXTI_Line = EXTI_LINEn;
    EXTI_InitStruct.EXTI_Trigger = EXTITrigger_x;
    EXTI_InitStruct.EXTI_LineCmd = state;
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_Init(&EXTI_InitStruct);
}


/**
 * @name: KEY_Init
 * @description: 按键初始化函数，用于扫描模式和中断模式
 * @param {*}
 * @return {*}
 */
void KEY_Init(void)
{
    KEY_GPIO_Init();

    KEY_EXTI_Init();

}

